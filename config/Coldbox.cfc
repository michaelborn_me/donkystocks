component{
	public function configure(){
		coldbox = {
			reinitPassword: "DONKY",
			customErrorTemplate: "/coldbox/system/includes/bugReport.cfm",
			handlersIndexAutoReload: true,
			environments = {
				// uses cgi.http_host
				development: "127.0.0.1",
				production: "donkystocks.app"
			}
		};
	}

	/**
	 * Custom Coldbox (or other) settings specifically for PROD environments.
	 */
	public function production(){
		coldbox.handlersIndexAutoReload = false;
	}

	/**
	 * Custom Coldbox (or other) settings specifically for DEV environments.
	 */
	public function development(){
	}
}