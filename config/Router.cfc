// Inherits from Router and FrameworkSuperType
component{
	function configure(){
			// Turn on Full URL Rewrites, no index.cfm in the URL
			setFullRewrites( true );

			// custom routes
			// route( "/search" ).to( "search.index" );
			
			// Routing by Convention
			route( "/:handler/:action?" ).end();
			// when I hit /search, the event that SHOULD get executed by default is search.index.
			// ACCORDING TO the coldbox docs: https://coldbox.ortusbooks.com/the-basics/routing/routing-dsl/routing-by-convention
			
	}
}