<cfoutput>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>DonkyStocks</title>
	<meta name="description" content="Stocks demo.">
	<meta name="author" content="Michael Born">

	<link href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" rel="stylesheet">
	<link href="/resources/css/theme.css" rel="stylesheet">
</head>
<body class="has-navbar-fixed-top">

	#renderView( "partials/nav" )#
	#renderView( "partials/header" )#

	<!---Container And Views --->
	<div class="container">
		<div class="section">
			#renderView()#
		</div>
	</div>

	#renderView( "partials/footer" )#

</body>
</html>
</cfoutput>
