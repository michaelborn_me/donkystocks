<footer class="footer">
  <div class="content has-text-centered">
    <p>
      <strong>DonkyStocks</strong> by <a href="https://michaelborn.me">Michael</a>.
    </p>
  </div>
</footer>