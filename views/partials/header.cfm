<cfoutput>
	<section class="hero is-primary is-small">
		<div class="hero-body">
			<div class="container">
				<h1 class="title">#prc.title#</h1>
				<cfif structKeyExists( prc, "subtitle" )>
					<h2 class="subtitle">#prc.subtitle#</h2>
				</cfif>
			</div>
		</div>
	</section>
</cfoutput>