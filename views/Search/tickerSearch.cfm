<cfoutput>
	<form action="#event.buildLink( 'search.index' )#">
		<div class="field is-grouped">
			<div class="control">
				<label class="label sr-only" for="ticker">Ticker Symbol</label>
				<input type="text" name="ticker" class="input" placeholder="Enter ticker symbol">
			</div>
			<div class="control">
				<input type="submit" name="submit" class="button is-primary" value="Search">
			</div>
		</div>
	</form>
</cfoutput>