# DonkyStocks

A dumb little CF app to view financial data for publicly traded companies. Huge thanks to the [FinancialModelingPrep API](https://financialmodelingprep.com/developer/docs/), without which this product would be much more difficult.

## Stories / TODO

* As an investor, I should be able to view Earnings Per Share and other valuation metrics for Microsoft in a table of values showing year-over-year performance. ❌
* As an investor, I should be able to toggle between table view and graph view for many (most? all?) important metrics, such as Book Value Per Share, Net Income Per Share, Earnings Per Share, and Price-To-Earnings Per Share, because this helps me visualize the improvement (or lack thereof) of a stock. ❌
* As a newbie investor, I should be able to type in a ticker symbol (e.g. MSFT) and browse financials from that point on because I don't want to retype the ticker symbol whenever I need to look at more data for that company. (e.g. seamless single-clicks to pull up multiple financial views for the same company.) ❌
* As an experienced investor, I should be able to save my favorite stocks because I don't want to have to search multiple times. ❌
* As an experienced investor, I should be able to save my favorite graphs / data points because I don't want to spend a lot of time searching for the same data to compare day after day. ❌
* As an internet user in 2020, I should be able to browse just as easily on my phone or tablet as on my desktop or laptop. ❌

## The Good News

> For all have sinned, and come short of the glory of God ([Romans 3:23](https://www.kingjamesbibleonline.org/Romans-3-23/))

> But God commendeth his love toward us, in that, while we were yet sinners, Christ died for us. ([Romans 5:8](https://www.kingjamesbibleonline.org/Romans-5-8))

> That if thou shalt confess with thy mouth the Lord Jesus, and shalt believe in thine heart that God hath raised him from the dead, thou shalt be saved. ([Romans 10:9](https://www.kingjamesbibleonline.org/Romans-10-9/))