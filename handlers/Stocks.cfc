component{
	property name="Stocky" inject="Stocky@cfStocky";

	/**
	 * GET /stocks/companyProfile
	*/
	public function companyProfile( event, rc, prc ){
		prc.title = "Company Profile";
		prc.data = Stocky.getCompanyProfile( "AAPL" );
		event.setView( "Stocks" );
	}
	/**
	 * GET /stocks/balanceSheet
	*/
	public function balanceSheet( event, rc, prc ){
		prc.title = "Balance Sheet";
		prc.data = Stocky.getBalanceSheetStatement( "AAPL" );
		event.setView( "Stocks" );
	}
	/**
	 * GET /stocks/financialStatement
	*/
	public function financialStatement( event, rc, prc ){
		prc.title = "Financial Statement";
		prc.data = Stocky.getFinancialStatement( "AAPL" );
		event.setView( "Stocks" );
	}
}